class GetOverlay extends Overlay{
                GeoPoint geo;
                @Override
                public void draw(Canvas canvas, MapView mapView, boolean arg2) {
                        super.draw(canvas, mapView, arg2);
                        if(geo==null){
                                return;
                        }
                        Log.d(toString(), arg2+"-------draw--");
                        Projection projection = mapView.getProjection(); 
                        // 把经纬度变换到相对于MapView左上角的屏幕像素坐标
                        Point point = projection.toPixels(geo, null); 
                        // 可在此处添加您的绘制代码
                        Paint paintText = new Paint();
                        paintText.setColor(Color.RED);
                        paintText.setTextSize(35);
                        canvas.drawText("●", point.x-9, point.y+13, paintText); // 绘制文本
                }

                @Override
                public boolean onTap(GeoPoint geo, MapView arg1) {
                        Log.d(this.toString(), geo.getLongitudeE6()/1E6+"----------"+geo.getLatitudeE6()/1E6);
                        return super.onTap(geo, arg1);
                }
                
        }

地图, View, Canvas, 屏幕, 像素